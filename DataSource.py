#!/usr/bin/python

import os

class Words():
    def __init__(self, language):
        self.language = language
        self.path = self.__getPath__()
        self.files = sorted(os.listdir(self.path))
        self.currentFile = self.__getFile__()
        self.currentLine = self.__getLine__()
        
    def __getPath__(self):
        for root, dirs, files in os.walk("txt"):  
            for directory in dirs:
                if "_mod" in directory and self.language in directory:
                    return os.path.join("txt",directory)

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def next(self):
        if len(self.currentLine) > 1:
            return self.currentLine.pop(0)
        else:
            temp = self.currentLine.pop(0)
            self.currentLine = self.__getLine__()
            return temp
    
    def __getFile__(self):
        if len(self.files) > 0:
            newfile = os.path.join(self.path,self.files.pop(0))
            return open(newfile ,'r')
        else:
            raise StopIteration()
    
    def __getLine__(self):
        newline = self.currentFile.readline().split()
        while newline == []:
            self.currentFile = self.__getFile__()
            newline = self.currentFile.readline().split()
        return newline



class Phrases():
    def __init__(self, language):
        self.language = language
        self.path = self.__getPath__()
        self.files = sorted(os.listdir(self.path))
        self.currentFile = self.__getFile__()
        
    def __getPath__(self):
        for root, dirs, files in os.walk("txt"):  
            for directory in dirs:
                if "_mod" in directory and self.language in directory:
                    return os.path.join("txt",directory)

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def next(self):
        return self.__getLine__()
    
    def __getFile__(self):
        if len(self.files) > 0:
            newfile = os.path.join(self.path,self.files.pop(0))
            return open(newfile ,'r')
        else:
            raise StopIteration()
    
    def __getLine__(self):
        newline = self.currentFile.readline().replace("\n","").strip()
        while newline == "":
            self.currentFile = self.__getFile__()
            newline = self.currentFile.readline().replace("\n","").strip()
        return newline
