#!/usr/bin/python

import os
from DataSource import Words, Phrases
import sqlite3
import string

class Teacher():
    def __init__(self):
        self.con = sqlite3.connect("main.db")
        self.c = self.con.cursor()
        self.translator = str.maketrans('', '', string.punctuation)
        
    def __createWordsTable__(self):
        self.con.execute("CREATE TABLE IF NOT EXISTS words (word TEXT NOT NULL, number INTEGER NOT NULL)")
    
    def __createTier0Table__(self):
        self.con.execute("CREATE TABLE IF NOT EXISTS tier0 (word TEXT NOT NULL, number INTEGER NOT NULL)")
        
    def __createTier1Table__(self):
        self.con.execute("CREATE TABLE IF NOT EXISTS tier1 (combo TEXT NOT NULL, number INTEGER NOT NULL)")
    
    def updateWordsTable(self):
        db = {}
        self.__createWordsTable__()
        for word in Words("es"):
            newword = word.translate(self.translator)
            if newword not in db:
                db[newword] = 1
            else:
                db[newword] += 1
        for word, number in db.items():
            self.c.execute("INSERT INTO words (word,number) VALUES (?, ?)", (word,number,))
        self.con.commit()
        
    def updateTier0Table(self):
        db = {}
        self.__createTier0Table__()
        for i, phrase in enumerate(Phrases('es')):
            words = phrase.translate(self.translator).split() 
            if len(words) > 0:
                word = words[0]
                if word not in db:
                    db[word] = 1
                else:
                    db[word] += 1
            else:
                pass
            
        for word, number in db.items():
            self.c.execute("INSERT INTO tier0 (word,number) VALUES (?, ?)", (word,number,))
        self.con.commit()
    
    def updateTier1Table(self):
        db = {}
        self.__createTier1Table__()
        word1 = ""
        word2 = ""
        for word in Words('es'):
            word1, word2 = word2, word.translate(self.translator)
            if word1 != "" and word2 != "":
                combination = " ".join([word1, word2])
                if combination not in db:
                    db[combination] = 1
                else:
                    db[combination] += 1
        for combo, number in db.items():
            self.c.execute("INSERT INTO tier1 (combo,number) VALUES (?, ?)", (combo,number,))
        self.con.commit()
        
        
    #----------------
    
x = Teacher()
#x.updateWordsTable()
#x.updateTier0Table()
x.updateTier1Table()
