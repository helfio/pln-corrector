#!/usr/bin/python

import os

counter = 0

for root, dirs, files in os.walk("txt"):  
    for directory in dirs:
        if "_mod" not in directory:
            newdirectory = os.path.join(root,directory+"_mod")
            if not os.path.exists(newdirectory):
                os.makedirs(newdirectory)
            for newroot, newdirs, newfiles, in os.walk(os.path.join("txt",directory)):
                for file in newfiles:
                    if counter < 10:
                        with open(os.path.join(newroot,file), 'r') as origin:
                            with open(os.path.join(newdirectory,file), 'w') as end:
                                for line in origin:
                                    if '<' in line or '>' in line or len(line) == 0:
                                        #print("BAD:",line)
                                        pass
                                    else:
                                        payload = line.replace('(','').replace(')','') 
                                        if len(payload.split()) > 0:
                                            end.write(payload)
                        #counter += 1
