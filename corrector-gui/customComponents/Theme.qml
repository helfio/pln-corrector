pragma Singleton
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls.Styles 1.4


QtObject {
// Multiplier
    property real multiplier: 1

//    Sizes
    property real sizeTiny: multiplier*Screen.devicePixelRatio*Screen.pixelDensity*0.5           // 0.5 mm
    property real sizeSmall: multiplier*Screen.devicePixelRatio*Screen.pixelDensity*5          // 5 mm
    property real sizeMedium: multiplier*Screen.devicePixelRatio*Screen.pixelDensity*30        // 30 mm
    property real sizeLarge: multiplier*Screen.devicePixelRatio*Screen.pixelDensity*50         // 50 mm
    property real sizeVeryLarge: multiplier*Screen.devicePixelRatio*Screen.pixelDensity*150    // 150 mm

//    Colors
    property color colorLightBackground: "white"
    property color colorPrimary: "#0069d0"
    property color colorSecondary: "#002f5d"
    property color colorDisabled: "grey"
    property color colorDarkBackground: "black"
    property color colorPass: "green"
    property color colorNoPass: "red"
    property color colorDiscrepancy: "orange"
    property color colorAlert: "red"

//    Status
    property int stepStatusPassed: 1
    property int stepStatusNoPassed: 2
    property int stepStatusTBD: 3

//    Font size
    property real fontSizeTiny: multiplier*Screen.devicePixelRatio*Screen.pixelDensity*3
    property real fontSizeSmall:  multiplier*Screen.devicePixelRatio*Screen.pixelDensity*4
    property real fontSizeNormal:  multiplier*Screen.devicePixelRatio*Screen.pixelDensity*4.5
    property real fontSizeLarge:  multiplier*Screen.devicePixelRatio*Screen.pixelDensity*5
    property real fontSizeVeryLarge: multiplier*Screen.devicePixelRatio*Screen.pixelDensity*6
    property real fontSizeHuge: multiplier*Screen.devicePixelRatio*Screen.pixelDensity*7



}
