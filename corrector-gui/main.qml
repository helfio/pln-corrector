import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import "customComponents" 1.0
import customComponents 1.0

Window {
    id: theWindow
    visible: true
    width: Theme.sizeVeryLarge
    maximumWidth: width
    minimumWidth: width
    height: Theme.sizeLarge
    maximumHeight: height
    minimumHeight: height
    title: qsTr("Corrector PLN")
    Connection{
        id: myConnection
        body: textBox.text
    }
    Item{
        id: topItem
        width: parent.width
        height: parent.height/3
        anchors{
            horizontalCenter: parent.horizontalCenter
            top: parent.top
        }
        property real buttonWidth: Theme.sizeMedium*1.5
        Button{
            id: centerButton
            width: parent.buttonWidth
            anchors{
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.verticalCenter
            }
            text: myConnection.centerButton
        }
        Button{
            id: leftButton
            width: parent.buttonWidth
            anchors{
                right: centerButton.left
                rightMargin: Theme.sizeSmall
                verticalCenter: centerButton.verticalCenter
            }
            text: myConnection.leftButton
        }
        Button{
            id: rightButton
            width: parent.buttonWidth
            anchors{
                left: centerButton.right
                leftMargin: Theme.sizeSmall
                verticalCenter: centerButton.verticalCenter
            }
            text: myConnection.rightButton
            onClicked: myConnection.rightButtonClicked()
        }
    }
    Item{
        id: bottomItem
        width: parent.width
        height: parent.height*2/3
        anchors{
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
        }
        Rectangle{
            color: "transparent"
            border.color: "grey"
            height: textBox.height+4
            width: textBox.width+10
            anchors{
                horizontalCenter: parent.horizontalCenter
                top: parent.top
//                topMargin: Theme.sizeSmall
            }
            TextEdit{
                id: textBox
                anchors.centerIn: parent
                width: theWindow.width*0.95
                height: bottomItem.height-Theme.sizeSmall
                cursorVisible: true
                text: "Ayer me picó una abe"
            }
        }
    }
}
