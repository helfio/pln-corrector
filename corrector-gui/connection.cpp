#include "connection.h"

Connection::Connection(QObject *parent) : QObject(parent)
{
    setLeftButton("abjita");
    setRightButton("abejorro");
    setCenterButton("abejonejo");

}

QString Connection::body(){
    return m_body;
}

void Connection::setBody(QString body){
    m_body = body;
}

QString Connection::leftButton(){
    return m_leftButton;
}

void Connection::setLeftButton(QString label){
    m_leftButton = label;
}

QString Connection::rightButton(){
    return m_rightButton;
}

void Connection::setRightButton(QString label){
    m_rightButton = label;
}

QString Connection::centerButton(){
    return m_centerButton;
}

void Connection::setCenterButton(QString label){
    m_centerButton = label;
}
