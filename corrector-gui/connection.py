from PySide2.QtCore import QObject, Property, Signal, Slot

class Connection(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)

        self._text = ''
        self._leftButton = 'abeja'
        self._centerButton = 'abejorro'
        self._rightButton = 'abejita'

    @Property('QString')
    def text(self):
        return self._text

    @text.setter
    def text(self, text):
        print(text)
        self._text = text

    @Property('QString')
    def leftButton(self):
        return self._leftButton

    @leftButton.setter
    def leftButton(self, leftButton):
        self._leftButton = leftButton
        
    @Property('QString')
    def centerButton(self):
        return self._centerButton

    @centerButton.setter
    def centerButton(self, centerButton):
        self._centerButton = centerButton
        
    @Property('QString')
    def rightButton(self):
        return self._rightButton

    @rightButton.setter
    def rightButton(self, rightButton):
        self._rightButton = rightButton

    def rightButtonClicked(self):
        print('righ button was clicked!')
