#ifndef CONNECTION_H
#define CONNECTION_H

#include <QObject>

class Connection : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString body READ body WRITE setBody NOTIFY bodyChanged)
    Q_PROPERTY(QString leftButton READ leftButton WRITE setLeftButton NOTIFY leftButtonChanged)
    Q_PROPERTY(QString rightButton READ rightButton WRITE setRightButton NOTIFY rightButtonChanged)
    Q_PROPERTY(QString centerButton READ centerButton WRITE setCenterButton NOTIFY centerButtonChanged)
public:
    explicit Connection(QObject *parent = nullptr);

    QString body();
    void setBody(QString body);

    QString leftButton();
    void setLeftButton(QString body);

    QString rightButton();
    void setRightButton(QString body);

    QString centerButton();
    void setCenterButton(QString body);

signals:
    void bodyChanged();
    void leftButtonChanged();
    void leftButtonClicked();
    void rightButtonChanged();
    void rightButtonClicked();
    void centerButtonChanged();
    void centerButtonClicked();

public slots:

private:
    QString m_leftButton;
    QString m_centerButton;
    QString m_rightButton;
    QString m_body;
};

#endif // CONNECTION_H
