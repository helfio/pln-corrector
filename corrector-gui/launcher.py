import sys, os
from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine, qmlRegisterType
from PySide2.QtCore import QUrl, QObject
from connection import Connection

app = QGuiApplication(sys.argv)
qmlRegisterType(Connection, 'Connection', 1, 0, 'Connection')
view = QQmlApplicationEngine()
current_path = os.path.abspath(os.path.dirname(__file__))
qmlFile = os.path.join(current_path, 'main.qml')
view.load(QUrl(qmlFile))
res = app.exec_()
del view
sys.exit(res)
